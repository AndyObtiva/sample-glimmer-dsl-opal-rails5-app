require 'glimmer-dsl-opal'

Document.ready? do
#   require 'samples/hello/hello_world'
#   require 'samples/hello/hello_combo'
#   require 'samples/hello/hello_computed'
#   require 'samples/hello/hello_list_single_selection'
#   require 'samples/hello/hello_list_multi_selection'
#   require 'samples/hello/hello_browser'
#   require 'samples/hello/hello_tab'
#   require 'samples/elaborate/login'
#   require 'samples/elaborate/tic_tac_toe'
  require 'samples/elaborate/contact_manager'
end
